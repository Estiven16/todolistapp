<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/crear_tarea', 'TareasController@create')->name('crear_tarea');
Route::post('/guardar_tarea', 'TareasController@store')->name('guardar_tarea');
Route::delete('/eliminar_tarea/{id}', 'TareasController@destroy')->name('eliminar_tarea');
Route::get('/ver_tarea/{id}', 'TareasController@show')->name('ver_tarea');
Route::get('/editar_tarea/{id}', 'TareasController@edit')->name('editar_tarea');
Route::post('/actualizar_tarea/{id}', 'TareasController@update')->name('actualizar_tarea');
Route::get('/tareas', 'TareasController@index')->name('tareas');
Route::get('/buscar', 'TareasController@search')->name('buscar');
Route::get('/ver_tarea', 'TareasController@show')->name('ver_tarea');