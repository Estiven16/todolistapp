@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
			<div class="row">
		        <div class="col-lg-12 margin-tb">
		            <div class="pull-left">
		                <h2>Tareas</h2>
		            </div>
		            <div class="float-right">
		                <a class="btn btn-success" href="{{ route('crear_tarea') }}">Crear Tarea</a>
		            </div>
		            <br><br>
		        </div>
		    </div>
   
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
   
    <table class="table table-bordered">
        <tr>
            <th>No</th>
            <th>Nombre</th>
            <th>Descripcion</th>
            <th>Categorias</th>
            <th width="200px"></th>
        </tr>
        @foreach ($tareas as $tarea)
        <tr>
            <td>{{ $tarea->id }}</td>
            <td>{{ $tarea->nombre }}</td>
            <td>{{ $tarea->descripcion }}</td>
            <td>
            @foreach($tarea->categorias as $categoria)
            <span class="badge badge-primary">{{ $categoria->nombre }}</span>
            @endforeach
        	</td>
            <td>
                <form action="{{ route('eliminar_tarea',$tarea->id) }}" method="POST">
                    <a class="btn btn-primary btn-sm" href="{{ route('editar_tarea',$tarea->id) }}">Editar</a>
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger btn-sm">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </table>
</div>
</div>
</div>
@endsection
