@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar Tarea') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('actualizar_tarea',$data['tarea']->id) }}">
                        @csrf
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ $data['tarea']->nombre }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="descripcion" class="col-md-4 col-form-label text-md-right">{{ __('Descripción de la tarea') }}</label>

                            <div class="col-md-6">
                            	<textarea  id="descripcion" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" required>{{ $data['tarea']->descripcion }}</textarea>
                            

                                @error('descripcion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="categorias" class="col-md-4 col-form-label text-md-right">{{ __('Categorias') }}</label>

                            <div class="col-md-6">
                            	<select multiple="multiple" name="categorias[]" class="form-control @error('categorias') is-invalid @enderror" id="categorias">
    								@foreach($data['categorias'] as $Categoria)
    								    <option value="{{$Categoria->id}}"  @foreach($data['tarea']->categorias as $key => $tarea_categoria) @if($Categoria->id == $tarea_categoria->id)selected="selected"@endif @endforeach >{{$Categoria->nombre}}</option>
    								@endforeach
								</select>
                                @error('categorias')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
