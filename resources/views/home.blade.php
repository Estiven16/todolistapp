@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Home</div>
                <div class="row" style="margin: 5%">
                    <div class="col-md-6">
                        <h4 class="text-center">Buscar</h4>
                        <div class="form-group">
                            <input type="text" name="tarea" id="tarea" placeholder="Escriba el nombre de la tarea" class="form-control">
                        </div>
                        <div id="tareas_list"></div>                    
                    </div>
                    <div class="col-md-6">
                        <div id="ver_tarea"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection