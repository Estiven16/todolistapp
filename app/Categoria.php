<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre'];

    public function tareass()
    {
        return $this->belongsToMany(Role::class, 'tareas_categorias');
    }
}
