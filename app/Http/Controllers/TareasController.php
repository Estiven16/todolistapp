<?php

namespace App\Http\Controllers;

use App\Tarea;
use App\Categoria;
use Illuminate\Http\Request;

class TareasController extends Controller
{
    public function index()
    {
        $tareas = Tarea::where('user_id','=',auth()->user()->id)->get();
        return view('tareas.index',compact('tareas'));
    }

    public function create()
    {
        $categorias = Categoria::all();
        return view('tareas.crear')->with('categorias', $categorias);
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        
        $requestData = $request->all();
        $requestData['user_id'] = auth()->user()->id;
        $tarea = New Tarea;
        $tarea->fill($requestData);
        $tarea->save();

        if(isset($requestData['categorias'])){
            $tarea->categorias()->sync( $requestData['categorias']);
        }

        return redirect()->route('tareas')->with('success','Tarea creada correctamente');
    }

    public function show(Request $request)
    {
        $output = '';

        if($request->ajax()) {
            
            $data = Tarea::where('nombre', 'LIKE', $request->tarea.'%')->where('user_id','=',auth()->user()->id)->get();

            if(count($data)>0){
                $output = '<div>';
                $output .= '<h4>Nombre</h4>';
                $output .= '<p>'.$data[0]->nombre.'</p>';
                $output .= '<h4>Descripción</h4>';
                $output .= '<p>'.$data[0]->descripcion.'</p>'; 
                $output .= '<h4>Categorias</h4>';
                $output .= '<p>';
                foreach ($data[0]->categorias as $categoria) {
                    $output .= '<span class="badge badge-primary">'.$categoria->nombre.'</span><br>';
                }
                $output .= '</p>';
                $output .= '</div>'; 
            }
            
        }

        return $output;
    }

    public function edit($id)
    {
        $tarea = Tarea::find($id);
        $categorias = Categoria::all();
        $data = ['categorias' => $categorias, 'tarea'=>$tarea ];
        return view('tareas.editar')->with('data', $data);
    }

    public function update(Request $request, $id)
    {
       
        $request->validate([
            'nombre' => 'required',
            'descripcion' => 'required',
        ]);
        
        $requestData = $request->all();
        $tarea = Tarea::find($id);
        $tarea->fill($requestData);
        $tarea->save();

        if(isset($requestData['categorias'])){
            $tarea->categorias()->sync( $requestData['categorias']);
        }

        return redirect()->route('tareas')->with('success','Tarea editada correctamente');
    }

    public function destroy($id)
    {
        $tarea = Tarea::find($id);
        $tarea->delete();
        return redirect()->route('tareas')->with('success','Tarea eliminada correctamente');
    }

    public function search(Request $request)
    {

        if($request->ajax()) {

            $data = Tarea::where('nombre', 'LIKE', $request->tarea.'%')
                         ->where('user_id','=',auth()->user()->id)
                         ->get();

            $output = '';

            if (count($data)>0) {
                $output = '<ul class="list-group" style="display: block; position: relative; z-index: 1">';
                // loop through the result array
                foreach ($data as $row){

                    $output .= '<li class="list-group-item">'.$row->nombre.'</li>';
                }
                $output .= '</ul>';
            }
            else {
                $output .= '<li class="list-group-item">'.'No results'.'</li>';
            }
            // return output result array
            return $output;
        }
    }
}
