<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model
{

	protected $fillable = ['nombre','descripcion','user_id'];

    public function categorias()
    {
        return $this->belongsToMany(Categoria::class, 'tareas_categorias');
    }
}
