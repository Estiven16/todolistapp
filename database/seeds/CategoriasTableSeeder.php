<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoria = new \App\Categoria([
            'nombre' => 'Backend'
            ]);
        $categoria->save();

        $categoria = new \App\Categoria([
            'nombre' => 'Frontend'
            ]);
        $categoria->save();

        $categoria = new \App\Categoria([
            'nombre' => 'Database'
            ]);
        $categoria->save();

        $categoria = new \App\Categoria([
            'nombre' => 'Data'
            ]);
        $categoria->save();

        $categoria = new \App\Categoria([
            'nombre' => 'Server'
            ]);
        $categoria->save();
    }
}
